package br.com.treinamento.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.dao.impl.ComicDaoImpl;
import br.com.treinamento.marvel.comic.Result;
import br.com.treinamento.service.impl.ComicServiceImpl;

/**
 * 
 * <p>
 * The class test of {@link ComicServiceImpl}
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
public class ComicServiceTest {

    /**
     * The string with JSON receives of the Marvel.
     */
    String json;

    /**
     * The service.
     */
    private ComicService comicService = new ComicServiceImpl();

    /**
     * The DAO.
     */
    private ComicDao dao = new ComicDaoImpl();

    /**
     * Prerequisite.
     */
    @Before
    public void setUp() {
        comicService.setDAO(dao);
        json =
            "{\"code\":200,\"status\":\"Ok\",\"copyright\":\"2016 MARVEL\",\"attributionText\":\"Data provided by Marvel. � 2016 MARVEL\",\"attributionHTML\":\"Data provided by Marvel. � 2016 MARVEL\",\"etag\":\"0b7724b6879dcc8006d4a86efe4afccf506d1d3a\",   \"data\":{      \"offset\":0,      \"limit\":20,      \"total\":1485,      \"count\":20,      \"results\":[         {            \"id\":1017100,            \"name\":\"A-Bomb (HAS)\",            \"description\":\"Rick Jones has been Hulk's best bud since day one, but now he's more than a friend...he's a teammate! Transformed by a Gamma energy explosion, A-Bomb's thick, armored skin is just as strong and powerful as it is blue. And when he curls into action, he uses it like a giant bowling ball of destruction! \",            \"modified\":\"2013-09-18T15:54:04-0400\",            \"thumbnail\":{               \"path\":\"http://i.annihil.us/u/prod/marvel/i/mg/3/20/5232158de5b16\",               \"extension\":\"jpg\"            },            \"resourceURI\":\"http://gateway.marvel.com/v1/public/characters/1017100\",            \"comics\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1017100/comics\",               \"items\":[               ],               \"returned\":0            },            \"series\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1017100/series\",               \"items\":[               ],               \"returned\":0            },            \"stories\":{               \"available\":1,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1017100/stories\",               \"items\":[                  {                     \"resourceURI\":\"http://gateway.marvel.com/v1/public/stories/105929\",                     \"name\":\"cover from Free Comic Book Day 2013 (Avengers/Hulk) (2013) #1\",                     \"type\":\"cover\"                  }               ],               \"returned\":1            },            \"events\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1017100/events\",               \"items\":[               ],               \"returned\":0            },            \"urls\":[               {                  \"type\":\"detail\",                  \"url\":\"http://marvel.com/characters/76/a-bomb?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               },               {                  \"type\":\"comiclink\",                  \"url\":\"http://marvel.com/comics/characters/1017100/a-bomb_has?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               }            ]         },         {            \"id\":1010699,            \"name\":\"Aaron Stack\",            \"description\":\"\",            \"modified\":\"1969-12-31T19:00:00-0500\",            \"thumbnail\":{               \"path\":\"http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available\",               \"extension\":\"jpg\"            },           \"resourceURI\":\"http://gateway.marvel.com/v1/public/characters/1010699\",           \"comics\":{              \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010699/comics\",               \"items\":[               ],               \"returned\":0            },            \"series\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010699/series\",               \"items\":[              ],               \"returned\":0            },            \"stories\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010699/stories\",              \"items\":[               ],               \"returned\":0            },            \"events\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010699/events\",               \"items\":[               ],               \"returned\":0            },            \"urls\":[              {                  \"type\":\"detail\",                  \"url\":\"http://marvel.com/characters/2809/aaron_stack?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               },               {                  \"type\":\"comiclink\",                  \"url\":\"http://marvel.com/comics/characters/1010699/aaron_stack?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"              }            ]         },         {            \"id\":1010846,            \"name\":\"Aegis (Trey Rollins)\",            \"description\":\"\",            \"modified\":\"1969-12-31T19:00:00-0500\",            \"thumbnail\":{               \"path\":\"http://i.annihil.us/u/prod/marvel/i/mg/5/e0/4c0035c9c425d\",               \"extension\":\"gif\"            },            \"resourceURI\":\"http://gateway.marvel.com/v1/public/characters/1010846\",            \"comics\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010846/comics\",               \"items\":[               ],               \"returned\":0            },            \"series\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010846/series\",               \"items\":[               ],               \"returned\":0            },           \"stories\":{              \"available\":0,              \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010846/stories\",               \"items\":[              ],               \"returned\":0            },            \"events\":{               \"available\":0,               \"collectionURI\":\"http://gateway.marvel.com/v1/public/characters/1010846/events\",               \"items\":[               ],               \"returned\":0            },            \"urls\":[               {                 \"type\":\"detail\",                 \"url\":\"http://marvel.com/characters/95/aegis?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               },               {                  \"type\":\"wiki\",                  \"url\":\"http://marvel.com/universe/Aegis_%28Trey_Rollins%29?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               },              {                  \"type\":\"comiclink\",                  \"url\":\"http://marvel.com/comics/characters/1010846/aegis_trey_rollins?utm_campaign=apiRef&utm_source=435154361e961e180ac5938483277330\"               }            ]         }      ]   }}";
    }

    /**
     * Test the load and the list of comics.
     */
    @Test
    public void loadAndListComic() {
        comicService.loadComic(json);

        Assert.assertEquals(comicService.listComics().size(), 3);

    }

    /**
     * Test adding a new comic.
     */
    @Test
    public void add() {
        comicService.loadComic(json);
        List<Result> comics = comicService.listComics();

        comicService.add(comics.get(0));

        Assert.assertEquals(comicService.listComics().size(), 4);
    }

    /**
     * Test removing a comic.
     */
    @Test
    public void remove() {
        comicService.loadComic(json);
        List<Result> comics = comicService.listComics();

        String idComic = String.valueOf(comics.get(0).getId());

        comicService.remove(idComic);

        for (Result comic : comicService.listComics()) {
            if (String.valueOf(comic.getId()).equals(idComic)) {
                Assert.fail();
            }
        }
    }

    /**
     * Test updating the comic.
     */
    @Test
    public void update() {
        comicService.loadComic(json);
        List<Result> comics = comicService.listComics();

        Result oldComic = comics.get(0);

        Integer idOldComic = oldComic.getId();
        String oldName = oldComic.getDescription();

        oldComic.setDescription("Teste");

        comicService.update(oldComic);

        for (Result comic : comicService.listComics()) {
            if (comic.getId() == idOldComic) {
                if (comic.getDescription().equals(oldName)) {
                    Assert.fail();
                }
            }
        }
    }
}
