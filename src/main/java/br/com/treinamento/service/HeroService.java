package br.com.treinamento.service;

import java.util.List;

import org.springframework.stereotype.Service;

import br.com.treinamento.dao.HeroDao;
import br.com.treinamento.marvel.hero.Result;

/**
 * 
 * <p>
 * Service to manipulate heroes informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@Service
public interface HeroService {

    /**
     * Name of service.
     */
    String NAME = "HeroService";

    /**
     * Load the heroes.
     * @param json information to load heroes.
     */
    void loadHero(String json);

    /**
     * List the heroes with informations reduces.
     * @return the list.
     */
    List<String> listReduceHeroes();

    /**
     * List the heroes with informations completes.
     * @return the list.
     */
    List<Result> listHeroes();

    /**
     * Add a new hero.
     * @param hero the new hero.
     */
    void add(Result hero);

    /**
     * Removes a hero.
     * @param idHero the id to remove.
     */
    void remove(String idHero);

    /**
     * Update all information of the hero.
     * @param hero the new information of the hero.
     */
    void update(Result hero);

    /**
     * Set the DAO.
     * @param dao the dao.
     */
    void setDAO(HeroDao dao);
}
