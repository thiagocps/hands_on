package br.com.treinamento.service;

import java.util.List;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.marvel.comic.Result;

/**
 * 
 * <p>
 * Service to manipulate comics informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
public interface ComicService {

    /**
     * The Service name.
     */
    String BEAN_NAME = "ComicService";

    /**
     * Load the comic.
     * @param json Information to load/save the comics.
     */
    void loadComic(String json);

    /**
     * List the comics with information reduce.
     * @return the list.
     */
    List<String> listReduceComics();

    /**
     * List the comics with information complete.
     * @return the list.
     */
    List<Result> listComics();

    /**
     * Add a new comic.
     * @param comic the new comic.
     */
    void add(Result comic);

    /**
     * Remove the comic.
     * @param idComic id to remove.
     */
    void remove(String idComic);

    /**
     * Update all informations with the comic.
     * @param comic the object with the new informations.
     */
    void update(Result comic);

    /**
     * Set a DAO
     * @param dao the dao object.
     */
    void setDAO(ComicDao dao);
}
