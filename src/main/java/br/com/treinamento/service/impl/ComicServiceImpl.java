package br.com.treinamento.service.impl;

import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.marvel.comic.Comics;
import br.com.treinamento.marvel.comic.Result;
import br.com.treinamento.service.ComicService;

/**
 * 
 * <p>
 * Implematation of service to manipulate heroes informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@Named(ComicService.BEAN_NAME)
public class ComicServiceImpl implements ComicService {

    @Autowired
    private ComicDao comicDao;

    @Override
    public void loadComic(String json) {
        Gson gson = new Gson();
        Comics comic = gson.fromJson(json, Comics.class);

        comicDao.loadComic(comic);
    }

    @Override
    public List<String> listReduceComics() {
        return comicDao.listReduceComic();
    }

    @Override
    public List<Result> listComics() {
        return comicDao.listComic();
    }

    @Override
    public void add(Result comic) {
        comicDao.add(comic);
    }

    @Override
    public void remove(String idComic) {
        comicDao.remove(idComic);
    }

    @Override
    public void update(Result comic) {
        comicDao.update(comic);
    }

    @Override
    public void setDAO(ComicDao dao) {
        this.comicDao = dao;
    }
}
