package br.com.treinamento.service.impl;

import java.util.List;

import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import br.com.treinamento.dao.HeroDao;
import br.com.treinamento.marvel.hero.Hero;
import br.com.treinamento.marvel.hero.Result;
import br.com.treinamento.service.HeroService;

/**
 * 
 * <p>
 * Implematation of service to manipulate heroes informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@Service
@Named(HeroService.NAME)
public class HeroServiceImpl implements HeroService {

    @Autowired
    private HeroDao heroDao;

    @Override
    public void loadHero(String json) {
        Gson gson = new Gson();
        Hero hero = gson.fromJson(json, Hero.class);

        heroDao.loadHero(hero);
    }

    @Override
    public List<String> listReduceHeroes() {
        return heroDao.listReduceHero();
    }

    @Override
    public List<Result> listHeroes() {
        return heroDao.listHero();
    }

    @Override
    public void add(Result hero) {
        heroDao.add(hero);
    }

    @Override
    public void remove(String idHero) {
        heroDao.remove(idHero);

    }

    @Override
    public void update(Result hero) {
        heroDao.update(hero);

    }

    @Override
    public void setDAO(HeroDao dao) {
        this.heroDao = dao;

    }
}
