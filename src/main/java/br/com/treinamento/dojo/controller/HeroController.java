package br.com.treinamento.dojo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.marvel.hero.Result;
import br.com.treinamento.service.HeroService;

/**
 * 
 * <p>
 * Controller to receive heroes informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@RestController
@RequestMapping("hero")
public class HeroController {

    /**
     * The Hero's Service.
     */
    @Autowired
    private HeroService heroService;

    /**
     * List the heroes with information reduce.
     * @return sucess.
     * @throws JsonProcessingException If the value not parsed to JSON.
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<String> listHeroes() throws JsonProcessingException {

        List<String> list = heroService.listReduceHeroes();

        ObjectMapper mapper = new ObjectMapper();

        return new ResponseEntity<String>(mapper.writeValueAsString(list), HttpStatus.OK);
    }

    /**
     * Add a new hero.
     * @param requestBody the new hero.
     * @return sucess.
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Object> add(@RequestBody Result requestBody) {
        heroService.add(requestBody);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Remove a hero.
     * @param idHero id to remove.
     * @return sucess.
     */
    @RequestMapping(value = "/remove/{idHero}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> remove(@PathVariable("idHero") String idHero) {
        heroService.remove(idHero);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Update the all information of the hero.
     * @param requestBody the object with the new informations.
     * @return sucess.
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Object> update(@RequestBody Result requestBody) {
        heroService.update(requestBody);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
