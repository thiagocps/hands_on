package br.com.treinamento.dojo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import br.com.treinamento.service.ComicService;
import br.com.treinamento.service.HeroService;
import br.com.treinamento.util.MakeUrl;
import br.com.treinamento.util.MakeUrl.URLType;

/**
 * 
 * <p>
 * Controller to receive main informations of the marvel.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@RestController
public class ConnectController {

    @Autowired
    private HeroService heroService;

    @Autowired
    private ComicService comicService;

    /**
     * Load the main information about heroes and comics.
     * @return sucess
     */
    @RequestMapping(value = "/connect", method = RequestMethod.GET)
    public ResponseEntity<String> connect() {
        Client c = Client.create();

        WebResource wr = c.resource(MakeUrl.factoryURL(URLType.CHARACTERS));
        String json = wr.get(String.class);

        heroService.loadHero(json);

        wr = c.resource(MakeUrl.factoryURL(URLType.COMICS));
        json = wr.get(String.class);

        comicService.loadComic(json);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
