package br.com.treinamento.dojo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.marvel.comic.Result;
import br.com.treinamento.service.ComicService;

/**
 * 
 * <p>
 * Controller to receive comics informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@RestController
@RequestMapping("comic")
public class ComicController {

    /**
     * The Comic Service.
     */
    @Autowired
    private ComicService comicService;

    /**
     * List the comics with reduce informations.
     * @return the list.
     * @throws JsonProcessingException If the value not parsed to JSON.
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<String> listComics() throws JsonProcessingException {

        List<String> list = comicService.listReduceComics();

        ObjectMapper mapper = new ObjectMapper();

        return new ResponseEntity<>(mapper.writeValueAsString(list), HttpStatus.OK);
    }

    /**
     * Add a new Comic.
     * @param requestBody a new comic to add.
     * @return sucess.
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Object> add(@RequestBody Result requestBody) {
        comicService.add(requestBody);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Remove the comic.
     * @param idComic id to remove.
     * @return sucess.
     */
    @RequestMapping(value = "/remove/{idComic}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> remove(@PathVariable("idComic") String idComic) {
        comicService.remove(idComic);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Update the comic with a new informations.
     * @param requestBody the object with a new information.
     * @return sucess.
     */
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Object> update(@RequestBody Result requestBody) {
        comicService.update(requestBody);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
