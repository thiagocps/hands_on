package br.com.treinamento.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

/**
 * 
 * <p>
 * The utility class to create a URL to connect in the Marvel.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
public class MakeUrl {

    /**
     * Variable to END_POINT_MARVEL.
     */
    private static final String END_POINT_MARVEL = "https://gateway.marvel.com:443/v1/public/";

    /**
     * Variable to PRIVATE_KEY.
     */
    private static final String PRIVATE_KEY = "8d90ca38428db41fb5817bfff04c8076aec48154";

    /**
     * Variable to PUBLIC_KEY.
     */
    private static final String PUBLIC_KEY = "435154361e961e180ac5938483277330";

    /**
     * Variable to TIMESTAMP.
     */
    private static final String TIMESTAMP = "?ts=";

    /**
     * Variable to API_KEY.
     */
    private static final String API_KEY = "&apikey=";

    /**
     * Variable to HASH.
     */
    private static final String HASH = "&hash=";

    /**
     * Factory the URL.
     * @param type the type of connection.
     * @return the URL link.
     */
    public static String factoryURL(URLType type) {
        Timestamp time = new Timestamp(System.currentTimeMillis());
        long timestamp = time.getTime();

        StringBuffer buffer = new StringBuffer();
        buffer.append(END_POINT_MARVEL);
        buffer.append(type);
        buffer.append(TIMESTAMP);
        buffer.append(timestamp);
        buffer.append(API_KEY);
        buffer.append(PUBLIC_KEY);
        buffer.append(HASH);
        buffer.append(createHashCodeMD5(timestamp));

        return buffer.toString();
    }

    /**
     * Create the hash MD5 code using the timestamp, public key and private key to connection with the Marvel.
     * @param timestamp Timestamp of the connection.
     * @return the MD5 code.
     */
    private static String createHashCodeMD5(long timestamp) {
        StringBuffer strToMD5 = new StringBuffer();
        strToMD5.append(timestamp);
        strToMD5.append(PRIVATE_KEY);
        strToMD5.append(PUBLIC_KEY);

        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
            md5.update(strToMD5.toString().getBytes(), 0, strToMD5.toString().length());
            return new BigInteger(1, md5.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 
     * <p>
     * Enum to limits choice in the connection.
     * </p>
     *
     * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
     *
     */
    public enum URLType {

        /** For heroes. */
        CHARACTERS("characters"),

            /** For comics. */
        COMICS("comics");

        /**
         * Type of the end point.
         */
        private String type;

        /**
         * Constructor.
         * @param type key.
         */
        URLType(String type) {
            this.type = type;
        }

        /**
         * Gets the attribute type.
         *
         * @return type.
         */
        public String getType() {
            return type;
        }

        /**
         * To string used in the composition to URL.
         * @return the type.
         */
        @Override
        public String toString() {
            return getType();
        }
    }
}
