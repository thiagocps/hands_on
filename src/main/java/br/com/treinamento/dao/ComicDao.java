package br.com.treinamento.dao;

import java.util.List;

import br.com.treinamento.marvel.comic.Comics;
import br.com.treinamento.marvel.comic.Result;

/**
 * 
 * <p>
 * DAO to save comics informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */

public interface ComicDao {

    /**
     * Name of DAO.
     */
    public static final String NAME = "ComicDao";

    /**
     * Load the comics.
     * @param comic the object to load/save in the memory.
     */
    void loadComic(Comics comic);

    /**
     * List with information reduce of comics.
     * @return the list.
     */
    List<String> listReduceComic();

    /**
     * list with information complete of comics.
     * @return the list.
     */
    List<Result> listComic();

    /**
     * Add a new comic.
     * @param comic the new comic to add.
     */
    void add(Result comic);

    /**
     * Remove the comic.
     * @param idComic id to remove.
     */
    void remove(String idComic);

    /**
     * Update all informations of comic.
     * @param comic the object with new informations.
     */
    void update(Result comic);
}
