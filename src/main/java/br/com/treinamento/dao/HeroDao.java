package br.com.treinamento.dao;

import java.util.List;

import br.com.treinamento.marvel.hero.Hero;
import br.com.treinamento.marvel.hero.Result;

/**
 * 
 * <p>
 * DAO to save heroes informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
public interface HeroDao {

    /**
     * The DAO name.
     */
    public static final String NAME = "HeroDao";

    /**
     * Load the heroes in the memory.
     * @param hero object to load/save in the memory.
     */
    void loadHero(Hero hero);

    /**
     * List with information complete with heroes.
     * @return the list.
     */
    List<Result> listHero();

    /**
     * List with information reduce with heroes.
     * @return the list.
     */
    List<String> listReduceHero();

    /**
     * Add a new hero.
     * @param hero the new hero.
     */
    void add(Result hero);

    /**
     * Remove a hero.
     * @param idHero ID to remove.
     */
    void remove(String idHero);

    /**
     * Update all informations of the hero.
     * @param hero Object with a new informations.
     */
    void update(Result hero);
}
