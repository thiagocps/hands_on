package br.com.treinamento.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import br.com.treinamento.dao.ComicDao;
import br.com.treinamento.marvel.comic.Comics;
import br.com.treinamento.marvel.comic.Result;

/**
 * 
 * <p>
 * DAO implematation to save comics informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@Named(ComicDao.NAME)
public class ComicDaoImpl implements ComicDao {

    private Comics comic;

    @Override
    public List<String> listReduceComic() {
        List<String> list = new ArrayList<>();
        if (comic != null) {
            for (Result r : comic.getData().getResults()) {
                list.add(r.getId() + " - " + r.getTitle());
            }
        }
        return list;
    }

    @Override
    public List<Result> listComic() {
        return comic.getData().getResults();
    }

    @Override
    public void loadComic(Comics comic) {
        this.comic = comic;
    }

    @Override
    public void add(Result comic) {
        this.comic.getData().getResults().add(comic);
    }

    @Override
    public void remove(String idComic) {
        for (int i = 0; i < comic.getData().getResults().size(); i++) {
            if (comic.getData().getResults().get(i).getId() == Integer.parseInt(idComic)) {
                comic.getData().getResults().remove(i);
            }
        }
    }

    @Override
    public void update(Result comic) {
        for (Result result : this.comic.getData().getResults()) {
            if (result.getId() == comic.getId()) {
                result.setCharacters(comic.getCharacters());
                result.setCollectedIssues(comic.getCollectedIssues());
                result.setCollections(comic.getCollections());
                result.setCreators(comic.getCreators());
                result.setDates(comic.getDates());
                result.setDescription(comic.getDescription());
                result.setDiamondCode(comic.getDiamondCode());
                result.setDigitalId(comic.getDigitalId());
                result.setEan(comic.getEan());
            }
        }
    }

}
