package br.com.treinamento.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import br.com.treinamento.dao.HeroDao;
import br.com.treinamento.marvel.hero.Hero;
import br.com.treinamento.marvel.hero.Result;

/**
 * 
 * <p>
 * DAO implematation to save comics informations.
 * </p>
 *
 * @author <a href="mailto:pr.thiagocps@gmail.com">Thiago</a>
 *
 */
@Named(HeroDao.NAME)
public class HeroDaoImpl implements HeroDao {

    private Hero hero;

    @Override
    public List<String> listReduceHero() {
        List<String> list = new ArrayList<>();
        if (hero != null) {
            for (Result r : hero.getData().getResults()) {
                list.add(r.getId() + " - " + r.getName());
            }
        }
        return list;
    }

    @Override
    public List<Result> listHero() {
        return hero.getData().getResults();
    }

    @Override
    public void loadHero(Hero hero) {
        this.hero = hero;
    }

    @Override
    public void add(Result hero) {
        this.hero.getData().getResults().add(hero);
    }

    @Override
    public void remove(String idHero) {
        for (int i = 0; i < hero.getData().getResults().size(); i++) {
            if (hero.getData().getResults().get(i).getId() == Integer.parseInt(idHero)) {
                hero.getData().getResults().remove(i);
            }
        }

    }

    @Override
    public void update(Result hero) {
        for (Result result : this.hero.getData().getResults()) {
            if (result.getId() == hero.getId()) {
                result.setComics(hero.getComics());
                result.setDescription(hero.getDescription());
                result.setEvents(hero.getEvents());
                result.setModified(hero.getModified());
                result.setName(hero.getName());
                result.setResourceURI(hero.getResourceURI());
                result.setSeries(hero.getSeries());
                result.setStories(hero.getStories());
                result.setThumbnail(hero.getThumbnail());
                result.setUrls(hero.getUrls());
            }
        }
    }

}
